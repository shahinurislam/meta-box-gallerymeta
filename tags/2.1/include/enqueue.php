<?php
function mbgm_metabox_enqueue($hook) {
    if ( 'post.php' == $hook || 'post-new.php' == $hook ) {
      wp_enqueue_script('mbgm-admin-js', plugin_dir_url( __DIR__ ). 'js/gallery-metabox.js', array('jquery'),'1.0.0',true);
      wp_enqueue_style('mbgm-admin-css', plugin_dir_url( __DIR__ ). 'css/gallery-metabox.css', array(), '1.0.0', 'all');
    }
  }
  add_action('admin_enqueue_scripts', 'mbgm_metabox_enqueue');
//-------------------------- Optional - If Add Shortcode use any single page with design ------------- //
function add_css_js(){    
    //front-end unityg
    wp_enqueue_style( 'unitycss', plugin_dir_url(__FILE__) . '../css/unite-gallery.css', array(), '1.0.0', 'all' );
    wp_enqueue_script('unityjs', plugin_dir_url(__FILE__) . '../js/unitegallery.min.js' , array('jquery'),'1.0.0',true);   
   if (is_singular('mb_gallery')) {
        //single
        wp_enqueue_script('unityjs2', plugin_dir_url(__FILE__) . '../js/ug-theme-tilesgrid.js' , array('jquery'),'1.0.0',true);
    }else{
        //blog
        wp_enqueue_script('ug-theme-slider', plugin_dir_url(__FILE__) . '../js/ug-theme-slider.js' , array('jquery'),'1.0.0',true);   
    }
    
}add_action('wp_enqueue_scripts','add_css_js');
?>