=== Meta-box GalleryMeta ===
Contributors: shahinurislam
Donate link: http://softcaff.xyz/me
Tags: image, gallery, new page
Requires at least: 5.4
Tested up to: 5.6
Stable tag: 1.0
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Just another gallery plugin. Simple but flexible.

== Description ==

Drag and drop multiple image upload by meta-box gallery for WordPress. Take full control over your WordPress site, build any gallery you can imagine – no programming knowledge required.

= Docs & Support =

You can find [docs](http://softcaff.xyz/gallerymeta/), [FAQ](http://softcaff.xyz/gallerymeta/) and more detailed information about Meta-box GalleryMeta on [softcaff.xyz](https://softcaff.xyz/). If you were unable to find the answer to your question on the FAQ or in any of the documentation, you should check the [support forum](https://wordpress.org/support/plugin/meta-box-gallery/) on WordPress.org. If you can't locate any topics that pertain to your particular issue, post a new topic for it.

= Meta-box GalleryMeta Needs Your Support =

It is hard to continue development and support for this free plugin without contributions from users like you. If you enjoy using Meta-box GalleryMeta and find it useful, please consider [__making a donation__](http://softcaff.xyz/gallerymeta/). Your donation will help encourage and support the plugin's continued development and better user support.

== Installation ==

1. Upload the entire `meta-box-gallery` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Contact' menu in your WordPress admin panel.

For basic usage, you can also have a look at the [plugin web site](https://softcaff.xyz/).

== Frequently Asked Questions ==

Do you have questions or issues with Contact Form 7? Use these support channels appropriately.

= Meta-box-gallery is responsive? =

Yes this plugin are fully responsive and support every mobile device.

= What about this plugin? =

This plugin is upload multiple image file as post type and use as gallery.
== Upgrade Notice ==

This is new version

== Screenshots ==

1. screenshot-1.png

== Changelog ==

= 1.0 =

* Add new post type as gallerymeta.
* Generate shortcode place and show the main file.