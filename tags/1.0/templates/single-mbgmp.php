<?php get_header();	
	while(have_posts())	: the_post();
?>
<style>
.arrowpagenav{display:none}
</style>
<section  data-scrollax-parent="true">
    <div class="container" id="btncontainer">
		<div class="row">
            <div class="d-grid gap-2 d-md-flex justify-content-md-end" id="btnback">
              <a href="<?php echo get_home_url(); ?>/album"><button class="btn btn-primary me-md-2" type="button"><?php esc_html_e( 'Back to Album', 'mbgm' ); ?></button></a>
            </div>
        </div>
    </div>
	<div class="container">
		<div class="row">
		 <script language="javascript">
			jQuery(document).ready(function() {
				//alert('d');
				jQuery('#swarupGallery').mycaptcha({
					width: '100',
					navHolder: '#navHolder',
					nextText: '',
					prevText: '',
					coloum: 12,
					gutterSpace: 10,
				});
				//jQuery(".group2").colorbox({rel:'group2', transition:"fade"});
			});
			</script>
			<div id="swarupGallery">
			  <ul>
				<?php
				global $post;
				//show front-end
				$images = get_post_meta($post->ID, 'mbgm_gallery_id', true);
				if (is_array($images) || is_object($images))
				{
					foreach ( $images as $image) {
				  //echo wp_get_attachment_link($image, 'large');
				  //captions
				   $image_obj = get_post($image);
				  ?>
				  <li><a href="<?php echo esc_url(wp_get_attachment_url( $image ));?>"><?php //echo  wp_get_attachment_image($image, 'full');?><img src="<?php echo esc_url(wp_get_attachment_url( $image ));?>" width="190px" height="190px" alt="<?php echo $image_obj->post_excerpt;?>" /></a></li>
				  <?php
					}
				}
				?>
				
			  </ul>
			</div>
		</div>
	</div>
</section>
<?php endwhile;  ?>
<?php get_footer()?>