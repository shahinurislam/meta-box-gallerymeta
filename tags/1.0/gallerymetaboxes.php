<?php
/*
Plugin Name: Meta-box GalleryMeta
Plugin URI: http://softcaff.xyz/gallerymeta
Description: Drag and drop multiple image upload by meta-box gallery for WordPress. Take full control over your WordPress site, build any gallery you can imagine – no programming knowledge required.
Version: 1.0.0
Author: Md. Shahinur Islam
Author URI: http://softcaff.xyz/me
*/
//--------------------- Create custom post type ---------------------------//
add_action( 'init', 'mbgm_gallery_post' );
function mbgm_gallery_post() {
	$labels = array(
		'name'               => _x( 'MB Gallery', 'post type general name', 'mbgm' ),
		'singular_name'      => _x( 'MB Gallery', 'post type singular name', 'mbgm' ),
		'menu_name'          => _x( 'MB Gallery Options', 'admin menu', 'mbgm' ),
		'name_admin_bar'     => _x( 'MB Gallery', 'add new on admin bar', 'mbgm' ),
		'add_new'            => _x( 'Add New', 'MB Gallery', 'mbgm' ),
		'add_new_item'       => __( 'Add New MB Gallery', 'mbgm' ),
		'new_item'           => __( 'New MB Gallery', 'mbgm' ),
		'edit_item'          => __( 'Edit MB Gallery', 'mbgm' ),
		'view_item'          => __( 'View MB Gallery', 'mbgm' ),
		'all_items'          => __( 'All MB Gallery', 'mbgm' ),
		'search_items'       => __( 'Search MB Gallery', 'mbgm' ),
		'parent_item_colon'  => __( 'Parent MB Gallery:', 'mbgm' ),
		'not_found'          => __( 'No MB Gallery found.', 'mbgm' ),
		'not_found_in_trash' => __( 'No MB Gallery found in Trash.', 'mbgm' )
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'mbgm' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'mbgmp' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
	register_post_type( 'mbgmp', $args );
}
//gallery Taxonomy
add_action( 'init', 'mbgm_taxonomy', 0 );
function mbgm_taxonomy() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'MB Gallery Category', 'taxonomy general name', 'mbgm' ),
		'singular_name'     => _x( 'MB Gallery Categories', 'taxonomy singular name', 'mbgm' ),
		'search_items'      => __( 'Search MB Gallery Categories', 'mbgm' ),
		'all_items'         => __( 'All MB Gallery', 'mbgm' ),
		'parent_item'       => __( 'Parent MB Gallery', 'mbgm' ),
		'parent_item_colon' => __( 'Parent MB Gallery:', 'mbgm' ),
		'edit_item'         => __( 'Edit MB Gallery', 'mbgm' ),
		'update_item'       => __( 'Update MB Gallery', 'mbgm' ),
		'add_new_item'      => __( 'Add New MB Gallery', 'mbgm' ),
		'new_item_name'     => __( 'New MB Gallery Name', 'mbgm' ),
		'menu_name'         => __( 'MB Gallery Categories', 'mbgm' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'mbgmc' ),
	);
	register_taxonomy( 'mbgmc', array( 'mbgmp' ), $args );
	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'MB Gallery Tags', 'taxonomy general name', 'mbgm' ),
		'singular_name'              => _x( 'MB Gallery', 'taxonomy singular name', 'mbgm' ),
		'search_items'               => __( 'Search MB Gallery Tags', 'mbgm' ),
		'popular_items'              => __( 'Popular MB Gallery Tags', 'mbgm' ),
		'all_items'                  => __( 'All MB Gallery Tags', 'mbgm' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit MB Gallery Tags', 'mbgm' ),
		'update_item'                => __( 'Update MB Gallery Tags', 'mbgm' ),
		'add_new_item'               => __( 'Add New MB Gallery Tags', 'mbgm' ),
		'new_item_name'              => __( 'New MB Gallery Name', 'mbgm' ),
		'separate_items_with_commas' => __( 'Separate MB Gallery with commas', 'mbgm' ),
		'add_or_remove_items'        => __( 'Add or remove MB Gallery', 'mbgm' ),
		'choose_from_most_used'      => __( 'Choose from the most used MB Gallery', 'mbgm' ),
		'not_found'                  => __( 'No MB Gallery found.', 'mbgm' ),
		'menu_name'                  => __( 'MB Gallery Tags', 'mbgm' ),
	);
	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'mbgmt' ),
	);
	register_taxonomy( 'mbgmt', 'mbgmp', $args );
}
//-------------- Load Custom post type Single page --------------------//
 function mbgm_single_template( $template ) {
    global $post;
    if ( 'mbgmp' === $post->post_type && locate_template( array( 'templates/single-mbgmp.php' ) ) !== $template ) {
        return plugin_dir_path( __FILE__ ) . 'templates/single-mbgmp.php';
    }
    return $template;
}
add_filter( 'single_template', 'mbgm_single_template' );
//--------------------------Create meta box filed as custom post -----------------------------//
  function mbgm_metabox_enqueue($hook) {
    if ( 'post.php' == $hook || 'post-new.php' == $hook ) {
      wp_enqueue_script('mbgm-admin-js', plugin_dir_url( __DIR__ ).'meta-box-gallery/js/gallery-metabox.js', array('jquery', 'jquery-ui-sortable'));
      wp_enqueue_style('mbgm-admin-css', plugin_dir_url( __DIR__ ).'meta-box-gallery/css/gallery-metabox.css');
    }
  }
  add_action('admin_enqueue_scripts', 'mbgm_metabox_enqueue');
  function add_mbgm($post_type) {
    $types = array('post', 'page', 'mbgmp');
    if (in_array($post_type, $types)) {
      add_meta_box(
        'mbgm',
        'Gallery',
        'mbgm_callback',
        $post_type,
        'normal',
        'high'
      );
    }
  }
  add_action('add_meta_boxes', 'add_mbgm');
  function mbgm_callback($post) {
    wp_nonce_field( basename(__FILE__), 'mbgm_meta_nonce' );
    $ids = get_post_meta($post->ID, 'mbgm_gallery_id', true);
    ?>
    <table class="form-table">
      <tr><td>
        <a class="gallery-add button" href="#" data-uploader-title="<?php esc_html_e( 'Add image(s) to gallery', 'mbgm' );?>" data-uploader-button-text="<?php esc_html_e( 'Add image(s)', 'mbgm' );?>"><?php esc_html_e( 'Add image(s)', 'mbgm' );?></a>
        <ul id="gallery-metabox-list">
        <?php if ($ids) : foreach ($ids as $key => $value) : $image = wp_get_attachment_image_src($value); ?>
          <li>
            <input type="hidden" name="mbgm_gallery_id[<?php echo $key; ?>]" value="<?php echo $value; ?>">
            <img class="image-preview" src="<?php echo esc_url($image[0]); ?>">
            <a class="change-image button button-small" href="#" data-uploader-title="<?php esc_html_e( 'Change image', 'mbgm' );?>" data-uploader-button-text="<?php esc_html_e( 'Change image', 'mbgm' );?>"><?php esc_html_e( 'Change image', 'mbgm' );?></a><br>
            <small><a class="remove-image" href="#"><?php esc_html_e( 'Remove image', 'mbgm' );?></a></small>
          </li>
        <?php endforeach; endif; ?>
        </ul>
      </td></tr>
    </table>
  <?php }
  function mbgm_save($post_id) {
    if (!isset($_POST['mbgm_meta_nonce']) || !wp_verify_nonce($_POST['mbgm_meta_nonce'], basename(__FILE__))) return;
    if (!current_user_can('edit_post', $post_id)) return;
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
    if(isset($_POST['mbgm_gallery_id'])) {
		$array = array_map( 'sanitize_text_field', wp_unslash( $_POST['mbgm_gallery_id'] ) );
		update_post_meta($post_id, 'mbgm_gallery_id', $array);
    } else {
      delete_post_meta($post_id, 'mbgm_gallery_id');
    }
  }
  add_action('save_post', 'mbgm_save');
//-------------------------- Optional - If Add Shortcode use any single page with design ------------- //
function mbgm_metabox_front() {
//jQuiry design css and js
  wp_enqueue_style('mbgm-front-js', plugin_dir_url( __DIR__ ).'meta-box-gallery/css/magnific-popup.css');
  wp_enqueue_style('mbgm-frontb-js', plugin_dir_url( __DIR__ ).'meta-box-gallery/css/bootstrap.css');
  //js
  wp_enqueue_script('mbgm-demo-js', plugin_dir_url( __DIR__ ).'meta-box-gallery/js/demo.js', array('jquery', 'jquery-ui-sortable'));
  wp_enqueue_script('mbgm-magnific-js', plugin_dir_url( __DIR__ ).'meta-box-gallery/js/jquery.magnific-popup.js', array('jquery', 'jquery-ui-sortable'));
}
add_action('wp_loaded', 'mbgm_metabox_front');
//-------------All post show------------//
function mbgm_shortcode_wrapper() {
	?>
	<section  data-scrollax-parent="true" class="dec-sec">
	<div class="container">
		<div class="row">
		<?php
		$metaboxesg_main_blog = new WP_Query(array(
			'post_type'=>'mbgmp'
		));
		if($metaboxesg_main_blog->have_posts())	:while($metaboxesg_main_blog->have_posts())	: $metaboxesg_main_blog->the_post(); ?>
            <!-- post-->
            <div class="col-lg-6 col-md-6">
            	<article class="post">
            		<div class="post-media">
            			<a href="<?php the_permalink();?>"><?php if(has_post_thumbnail()) the_post_thumbnail();?></a>
            		</div>
            		<div class="post-item fl-wrap">
            			<h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4> 												
            		</div>
            	</article>
            </div>
            <!-- post end -->
			<?php endwhile;	endif;?>
		</div>
	</div>
</section>
<?php
}
add_shortcode('mbgmwrapper','mbgm_shortcode_wrapper');
// Dashboard Front Show settings page
function mbgm_register_settings() {
   add_option( 'mbgm_option_name', 'This is my option value.');
   register_setting( 'mbgm_options_group', 'mbgm_option_name', 'mbgms_callback' );
}
add_action( 'admin_init', 'mbgm_register_settings' );
function mbgm_register_options_page() {
  add_options_page('Page Title', 'Meta-box GalleryMeta', 'manage_options', 'mbgm', 'mbgm_options_page');
}
add_action('admin_menu', 'mbgm_register_options_page');
 function mbgm_options_page()
{
?>
  <div>
  <h2><?php esc_html_e( 'Meta-box GalleryMeta Plugin', 'mbgm' );?></h2>
  <h3><?php esc_html_e( 'Copy and paste this shortcode here:', 'mbgm' );?></h3>
  <p><?php esc_html_e( '[mbgmwrapper]', 'mbgm' );?></p>
  <Button><a href="<?php echo get_home_url();?>/wp-admin/"><?php esc_html_e( 'OK', 'mbgm' );?></a></Button>
  </div>
<?php
} ?>
<?php
register_activation_hook(__FILE__, 'mbgm_plugin_activate');
add_action('admin_init', 'mbgm_plugin_redirect');
function mbgm_plugin_activate() {
    add_option('mbgm_plugin_do_activation_redirect', true);
}
function mbgm_plugin_redirect() {
    if (get_option('mbgm_plugin_do_activation_redirect', false)) {
        delete_option('mbgm_plugin_do_activation_redirect');
        if(!isset($_GET['activate-multi']))
        {
            wp_redirect("options-general.php?page=mbgm");
        }
    }
}
//side setting link
add_filter( 'plugin_action_links', 'mbgm_plugin_action_links', 10, 2 );
function mbgm_plugin_action_links( $glinks ) {
	$settings_glinks  = sprintf( '<a href="'.get_home_url().'/wp-admin/options-general.php?page=mbgm">%2$s</a>',
		menu_page_url( '', false ),
		esc_html( __( 'Settings', 'mbgm' ) ) );
	array_unshift( $glinks, $settings_glinks );
	return $glinks;
}