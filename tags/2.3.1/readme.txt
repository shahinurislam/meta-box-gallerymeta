=== Meta-box GalleryMeta ===
Contributors: shahinurislam
Donate link: https://forms.gle/EAtaCDDDxhcU5fva7
Tags: image, gallery, new page
Requires at least: 5.8
Tested up to: 6.5.2
Stable tag: 2.3.1
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Just another gallery plugin. Simple but flexible.

== Description ==

Drag and drop multiple image upload by meta-box gallery for WordPress. Take full control over your WordPress site, build any gallery you can imagine – no programming knowledge required.

= Docs & Support =

You can find [docs](https://wordpress.org/plugins/meta-box-gallerymeta/), [FAQ](https://wordpress.org/plugins/meta-box-gallerymeta/) and more detailed information about Meta-box GalleryMeta on [gitlab](https://gitlab.com/shahinurislam/meta-box-gallerymeta). If you were unable to find the answer to your question on the FAQ or in any of the documentation, you should check the [support forum](https://wordpress.org/support/plugin/meta-box-gallery/) on WordPress.org. If you can't locate any topics that pertain to your particular issue, post a new topic for it.

= Meta-box GalleryMeta Needs Your Support =

It is hard to continue development and support for this free plugin without contributions from users like you. If you enjoy using Meta-box GalleryMeta and find it useful, please consider [__making a donation__](https://forms.gle/EAtaCDDDxhcU5fva7). Your donation will help encourage and support the plugin's continued development and better user support. Find on gitlab. [Gitlab](https://gitlab.com/shahinurislam/meta-box-gallerymeta) 

== Installation ==

1. Upload the entire `meta-box-gallery` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Contact' menu in your WordPress admin panel.

For basic usage, you can also have a look at the [plugin web site](https://wordpress.org/plugins/meta-box-gallerymeta/).

== Frequently Asked Questions ==

Do you have questions or issues with Meta-box GalleryMeta? Use these support channels appropriately.

= Meta-box-gallery is responsive? =

Yes this plugin are fully responsive and support every mobile device.

= What about this plugin? =

This plugin is upload multiple image file as post type and use as gallery.

== Upgrade Notice ==

This is new version 2.3.1

== Screenshots ==

1. screenshot-1.png
2. screenshot-2.png
3. screenshot-3.png
4. screenshot-4.png
5. screenshot-5.png
6. screenshot-6.png

== Changelog ==

= 2.3.1 =

* WP Version Update.

= 2.3 =

* Bug Fix.
* Update WP.
* Slider add.
* 5 Slider add.

= 2.2.1 =

* Bug Fix.
* Update WP.

= 2.2 =

* Bug Fix.
* Update WP.

= 2.0 =

* Add new slide and video gallery support.
* Add new admin panel for new layout and many more

= 1.0 =

* Add new post type as gallerymeta.
* Generate shortcode place and show the main file.