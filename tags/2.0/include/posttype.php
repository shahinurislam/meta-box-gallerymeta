<?php

add_action( 'init', 'mbgmnew_gallery_post' );
function mbgmnew_gallery_post() {
	$labels = array(
		'name'               => _x( 'MB Gallery', 'post type general name', 'mbgm' ),
		'singular_name'      => _x( 'MB Gallery', 'post type singular name', 'mbgm' ),
		'menu_name'          => _x( 'MB Gallery', 'admin menu', 'mbgm' ),
		'name_admin_bar'     => _x( 'MB Gallery', 'add new on admin bar', 'mbgm' ),
		'add_new'            => _x( 'Add New', 'MB Gallery', 'mbgm' ),
		'add_new_item'       => __( 'Add New MBG', 'mbgm' ),
		'new_item'           => __( 'New MBG', 'mbgm' ),
		'edit_item'          => __( 'Edit MBG', 'mbgm' ),
		'view_item'          => __( 'View MBG', 'mbgm' ),
		'all_items'          => __( 'All MBG', 'mbgm' ),
		'search_items'       => __( 'Search MBG', 'mbgm' ),
		'parent_item_colon'  => __( 'Parent MBG:', 'mbgm' ),
		'not_found'          => __( 'No MBG found.', 'mbgm' ),
		'not_found_in_trash' => __( 'No MBG found in Trash.', 'mbgm' )
	);
	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'mbgm' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'mb_gallery' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 20,
		'menu_icon'           => 'dashicons-schedule',
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
	register_post_type( 'mb_gallery', $args );
}
//gallery Taxonomy
add_action( 'init', 'mbgm_taxonomy', 0 );
function mbgm_taxonomy() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'MBG Category', 'taxonomy general name', 'mbgm' ),
		'singular_name'     => _x( 'MBG Categories', 'taxonomy singular name', 'mbgm' ),
		'search_items'      => __( 'Search MBG Categories', 'mbgm' ),
		'all_items'         => __( 'All MBG', 'mbgm' ),
		'parent_item'       => __( 'Parent MBG', 'mbgm' ),
		'parent_item_colon' => __( 'Parent MBG:', 'mbgm' ),
		'edit_item'         => __( 'Edit MBG', 'mbgm' ),
		'update_item'       => __( 'Update MBG', 'mbgm' ),
		'add_new_item'      => __( 'Add New MBG', 'mbgm' ),
		'new_item_name'     => __( 'New MBG', 'mbgm' ),
		'menu_name'         => __( 'MBG Categories', 'mbgm' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'mbgmc' ),
	);
	register_taxonomy( 'mbg_category', array( 'mb_gallery' ), $args );
	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'MBG Tags', 'taxonomy general name', 'mbgm' ),
		'singular_name'              => _x( 'MBG', 'taxonomy singular name', 'mbgm' ),
		'search_items'               => __( 'Search MBG Tags', 'mbgm' ),
		'popular_items'              => __( 'Popular MBG Tags', 'mbgm' ),
		'all_items'                  => __( 'All MBG Tags', 'mbgm' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit MBG Tags', 'mbgm' ),
		'update_item'                => __( 'Update MBG Tags', 'mbgm' ),
		'add_new_item'               => __( 'Add New MBG Tags', 'mbgm' ),
		'new_item_name'              => __( 'New MBG', 'mbgm' ),
		'separate_items_with_commas' => __( 'Separate MBG commas', 'mbgm' ),
		'add_or_remove_items'        => __( 'Add or remove MBG', 'mbgm' ),
		'choose_from_most_used'      => __( 'Choose from the most used MBG', 'mbgm' ),
		'not_found'                  => __( 'No MBG found.', 'mbgm' ),
		'menu_name'                  => __( 'MBG Tags', 'mbgm' ),
	);
	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'mbgmt' ),
	);
	register_taxonomy( 'mbg_tag', 'mb_gallery', $args );
}
?>